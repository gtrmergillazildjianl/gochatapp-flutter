import 'package:flutter/material.dart';
import './pages/route.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        //primaryColor: Color.fromRGBO(0x21, 0x21, 0x21, 1),
        //primaryColorLight: Color.fromRGBO(0x48, 0x48, 0x48, 1),
        //primaryColorDark: Color.fromRGBO(0, 0, 0, 1),
        //accentColor: Color.fromRGBO(0x26, 0x32, 0x38, 1),
        colorScheme: ColorScheme.dark(
          primary: Color.fromRGBO(0x21, 0x21, 0x21, 1),
          primaryVariant: Color.fromRGBO(0, 0, 0, 1),
          secondary: Color.fromRGBO(0x26, 0x32, 0x38, 1),
          secondaryVariant: Color.fromRGBO(0x00, 0x0A, 0x12, 1),
          onPrimary: Colors.white,
          onSecondary: Colors.white,
          surface: Color.fromRGBO(0, 0, 0, 1),
          onSurface: Colors.white,
          onBackground: Colors.white,
          background: Color.fromRGBO(0x21, 0x21, 0x21, 1),
          brightness: Brightness.dark,
        ),
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      onGenerateRoute: generateRoutes,
      initialRoute: "/login",
    );
  }
}