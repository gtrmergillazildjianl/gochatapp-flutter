import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoginButton extends StatefulWidget {

  final void Function() onPressed;
  final Color backgroundColor;
  final Color onPressedColor;

  LoginButton({
    this.onPressed,
    this.backgroundColor,
    this.onPressedColor
  });


  @override
  _LoginButtonState createState() => _LoginButtonState();
}

class _LoginButtonState extends State<LoginButton> {

  static final double _SHADOW_OPACITY = 0.5;

  double _shadowOpacity;
  Color _backgroundColor;

  @override
  void initState() {
    super.initState();
    _backgroundColor = widget.backgroundColor;
    _shadowOpacity = _SHADOW_OPACITY;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTapDown: _onTapDown,
      onTapUp: _onTapUp,
      onTapCancel: _onTapCancel,
      child: AnimatedContainer(
        duration: Duration(milliseconds: 200),
        padding: EdgeInsets.all(15),
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
                color: Theme.of(context).colorScheme.secondaryVariant.withOpacity(_shadowOpacity),
                spreadRadius: 1,
                blurRadius: 3,
                offset: Offset(3, 3))
          ],
          color: _backgroundColor,
          borderRadius: BorderRadius.all(Radius.circular(5)),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: EdgeInsets.only(right: 20),
              child: Image(
                  image: AssetImage("assets/icon/g-sign-in.png"), width: 25.0),
            ),
            Text("Sign in with Google",
                style: TextStyle(
                    color: Theme.of(context).colorScheme.onPrimary,
                    fontWeight: FontWeight.bold))
          ],
        ),
      ),
    );
  }

  void _onTapDown(TapDownDetails details){
    setState(() {
      _shadowOpacity = 0;
      _backgroundColor = widget.onPressedColor;
    });
    widget.onPressed();
  }

  void _onTapUp(TapUpDetails details) {
    setState(() {
      _shadowOpacity = _SHADOW_OPACITY;
      _backgroundColor = widget.backgroundColor;
    });
  }

  void _onTapCancel() {
    _onTapUp(null);
  }
}
