import 'package:flutter/material.dart';

class Header extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: MediaQuery.of(context).size.height * 0.65,
        child: CustomPaint(
          painter: HeaderPainter(context),
          child: Align(
            alignment: Alignment.bottomCenter,
            child: Stack(
              alignment: Alignment.center,
              children: [
                CircleAvatar(
                  radius: 80,
                  backgroundColor:
                      Theme.of(context).colorScheme.secondaryVariant,
                ),
                CircleAvatar(
                  radius: 70,
                  backgroundColor: Theme.of(context).colorScheme.primary,
                ),
                Image(
                    height: 120,
                    image: AssetImage("assets/icon/chat-bubble.png"))
              ],
            ),
          ),
        ));
  }
}

class HeaderPainter extends CustomPainter {
  final BuildContext context;

  HeaderPainter(this.context);

  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint();
    var path = Path();
    paint.color = Theme.of(context).colorScheme.secondaryVariant;
    paint.style = PaintingStyle.fill;
    paint.strokeWidth = 3.0;
    path.moveTo(0, 0);
    path.lineTo(0, size.height - 80);
    path.lineTo(size.width / 2 - 90, size.height - 80);
    //draw arc here
    path.arcToPoint(Offset(size.width / 2 + 90, size.height - 80),
        radius: Radius.circular(70), clockwise: true);
    //path.moveTo(size.width/2 + 90, size.height - 75);
    path.lineTo(size.width, size.height - 80);
    path.lineTo(size.width, 0);
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
