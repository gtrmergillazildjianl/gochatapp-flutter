import 'package:flutter/material.dart';
import 'package:gochat/pages/login/widget/loginButton.dart';
import 'package:gochat/pages/login/widget/header.dart';

class LoginPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        
        children: [
          Header(),
          Spacer(flex: 2),
          Center(child: LoginButton(
            backgroundColor: Theme.of(context).colorScheme.surface,
            onPressedColor: Theme.of(context).colorScheme.surface.withOpacity(0.8),
            onPressed: () => {
              print("pressed")
            },
          )),
          Spacer(
            flex: 1,
          ),
        ],
      )
    );
  }
}