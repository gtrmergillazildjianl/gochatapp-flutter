

import 'package:flutter/material.dart';
import 'package:gochat/pages/home/home.dart';
import 'package:gochat/pages/login/login.dart';

Route<dynamic> generateRoutes(RouteSettings settings) {
  switch(settings.name) {
    case "/login":
      return MaterialPageRoute(builder: (_) => LoginPage());
    default:
      return MaterialPageRoute(builder: (_) => HomePage());
  }
}